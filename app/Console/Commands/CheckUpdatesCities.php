<?php

namespace App\Console\Commands;

use App\Helpers\File;
use Illuminate\Console\Command;

class CheckUpdatesCities extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check-updates:cities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = new File();

        $url = "http://download.geonames.org/export/dump/RU.zip";

        $file->checkUpdatesData($url);
    }
}
