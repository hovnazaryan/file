<?php

namespace App\Console\Commands;

use App\City;
use Elasticsearch\ClientBuilder;
use Illuminate\Console\Command;
use Sleimanx2\Plastic\Facades\Plastic;

class ImportCities extends Command
{
    protected $index = 'cities_index';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:cities';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $limit = 300;

        if ($this->confirm('Would you like to do additional settings? [yes|no]')) {
            if ($this->confirm('Delete index '. $this->index . '? [yes|no]')) {
                $this->deleteIndex();
            }
            if ($this->confirm('Add index '. $this->index . '? [yes|no]')) {
                $this->createIndex();
            }
        }

        if ($this->confirm('Index Data '. $this->index . '? [yes|no]')) {
            $this->processCities($limit);
        }
    }

    protected function processCities($limit)
    {
        $this->info("Start indexing...");

        City::chunk($limit, function ($city) use ($limit) {
            foreach($city as $k=>$v) {
                $city[$k]->document()->save();
            }
            $this->info("Indexed " . count($city) . " '. $this->index . ' to ES  from {$city->first()->id} to {$city->last()->id}.");
        });
    }

    /**
     * Delete the index
     */
    protected function deleteIndex(){
        $client = ClientBuilder::create()->setHosts(config('plastic.connection.hosts'))->build();
        $params = ['index' => $this->index];
        $response = $client->indices()->delete($params);

        $this->info("Delete the index - response " . json_encode($response));
    }

    /**
     * Create the index
     */
    protected function createIndex(){
        $client = ClientBuilder::create()->setHosts(config('plastic.connection.hosts'))->build();

        $params = [
            'index' => $this->index,
            'body' => [
                "settings" => [
                    "analysis" => [
                        "filter" => [
                            "edge_ngram_filter" => [
                                "type" => "edge_ngram",
                                "min_gram" => 1,
                                "max_gram" => 15,
                                "token_chars" => [
                                    "letter",
                                    "digit"
                                ]
                            ]
                        ],
                        "analyzer" => [
                            "edge_ngram_analyzer" => [
                                "type" => "custom",
                                "tokenizer" => "whitespace",
                                "filter" => [
                                    "lowercase",
                                    "edge_ngram_filter"
                                ]
                            ],
                            "whitespace_analyzer" => [
                                "type" => "custom",
                                "tokenizer" => "whitespace",
                                "filter" => [
                                    "lowercase"
                                ]
                            ]
                        ],

                    ]
                ],
                "mappings"=> [
                    $this->index => [
                        '_all' => [
                            "type" => "completion",
                            "analyzer" => "edge_ngram_analyzer",
                            "search_analyzer" => "whitespace_analyzer"
                        ]
                    ]
                ]
            ]
        ];

        $response = $client->indices()->create($params);

        $this->info("Create the index - response " . json_encode($response));
    }
}
