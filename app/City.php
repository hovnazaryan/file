<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sleimanx2\Plastic\Searchable;
use Illuminate\Support\Facades\DB;

class City extends Model
{
    use Searchable;

    public $timestamps = false;

    public $searchable = ['name'];
    public $documentIndex = 'cities_index';

    public function buildDocument()
    {
        return [
            'name' => $this->name
        ];
    }


    public static function nearestCities($name)
    {
        $originalCity = City::query()->select('latitude', 'longitude')->whereName($name)->first();

        $latitude = $originalCity->latitude;
        $longitude = $originalCity->longitude;

        $data = City::select(DB::raw('*, ( 6367 * acos( cos( radians('.$latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( latitude ) ) ) ) AS distance'))
            ->having('distance', '<', 100)
            ->orderBy('distance')
            ->limit(20)
            ->get();

        return $data;
    }
}
