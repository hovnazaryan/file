<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = ['cities_table_dump_date'];


    /**
     * @param $fileTime
     */

    public static function createOrUpdateCityLastDate($fileTime)
    {
        $settings = self::query()->first();
        $data = ['cities_table_dump_date' => $fileTime];
        !empty($settings) ? $settings->update($data) : self::query()->create($data);
    }
}
