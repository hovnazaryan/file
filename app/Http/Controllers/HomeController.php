<?php

namespace App\Http\Controllers;

use App\City;
use App\Helpers\Search;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request) {
        return view('welcome');
    }


    /**
     * @param $name
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNearestCities ($name) {
        $cities = City::nearestCities($name);
        return response()->json(['cities' => $cities]);
    }


    /**
     * @param $keyword
     * @return \Illuminate\Http\JsonResponse
     */

    public function autoComplete($keyword)
    {
        $suggestion = Search::autoCompleteCity($keyword);
        return response()->json(['cities' => $suggestion->pluck('name')]);
    }
}
