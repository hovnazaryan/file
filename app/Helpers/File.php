<?php

namespace App\Helpers;


use App\City;
use App\Console\Commands\ImportCities;
use App\Settings;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Mockery\Exception;
use ZanySoft\Zip\Zip;

class File
{
    protected $old_date_dump_cities  = null;
    protected $fileName = 'cities';
    protected $process = false;

    public function checkUpdatesData($url)
    {
        if($this->process) {
            return false;
        }

        $client = new Client();
        $res = $client->get($url);

        if($this->old_date_dump_cities === null) {
            $oldDumpDate = Settings::query()->select('cities_table_dump_date')->first();

            if(!empty($oldDumpDate)) {
                $this->old_date_dump_cities = $oldDumpDate->cities_table_dump_date;
            }
        }

        $zipFileName = $this->fileName . '.zip';
        $headers = $res->getHeaders();
        $fileTime = null;

        if(isset($headers['Last-Modified']) && isset($headers['Last-Modified'][0]))
        {
            $fileTime =  Carbon::parse($headers['Last-Modified'][0])->format('Y-m-d H:i:s');
            $oldDate  = is_null($this->old_date_dump_cities) ? null : new Carbon($this->old_date_dump_cities);

            if(is_null($oldDate) || (($fileTime > $oldDate) && !is_null($fileTime))) {
                Storage::put('public/temp/' . $zipFileName , $res->getBody());

                $extractFileName = $this->extractFile($zipFileName);

                if(!empty($extractFileName))
                {
                    if($this->txtImporter()) {
                        Settings::createOrUpdateCityLastDate($fileTime);

                        $this->removeTempFiles();
                        $this->process = false;
                        return response('Successfully Imported');
                    }
                }
            } else {
                return response('Already up do date');
            }
        } else {
            return response('Already up do date');
        }
    }


    /**
     * Remove temp/unzip/downloaded files
     */

    public function removeTempFiles()
    {
        $tempDirPublic = public_path('temp');
        $tempDirStorage = 'public/temp';

        if (is_dir($tempDirPublic)) {
            $this->deleteDirectory($tempDirPublic);
        }

        return !Storage::exists($tempDirStorage) ? : Storage::deleteDirectory($tempDirStorage);
    }

    /**
     * @param string $fileName
     */

    protected function extractFile($fileName)
    {
        try {
            $zip = Zip::open(public_path('storage/temp/' . $fileName));
            $zip->extract(public_path('temp'));

            return 'RU.txt';
        } catch (Exception $exception) {
            throw  new Exception($exception->getMessage());
        }
    }

    /**
     * @param string $fileNameCsv
     */

    protected function txtImporter()
    {
        $this->process = true;

        City::query()->truncate();

        $file = str_replace('\\', '/', public_path('temp/RU.txt'));

        $query = "LOAD DATA LOCAL INFILE '" . $file . "'
                INTO TABLE cities
                    (geonameid,
                    name,
                    asciiname,
                    alternatenames,
                    latitude,
                    longitude,
                    feature_class,
                    feature_code,
                    country_code,
                    cc2,
                    admin1_code,
                    admin2_code,
                    admin3_code,
                    admin4_code,
                    population,
                    elevation,
                    dem,
                    timezone,
                    modification_date
        )";

        return DB::connection()->getpdo()->exec($query);
    }


    protected function deleteDirectory($dir) {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!$this->deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }

        }

        return rmdir($dir);
    }
}