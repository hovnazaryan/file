<?php

namespace App\Helpers;

class Search
{

    public static function autoCompleteCity($keyword)
    {
        $search = \App\City::search();

        $options = [
            "boost" => 1.0,
            "fuzziness" => 2,
            "prefix_length" => 0,
            "max_expansions" => 100
        ];

        return $search->fuzzy('name', $keyword, $options)->get()->hits();
    }
}