$(document).ready(function () {
    var markersArray = [];

    $.ajaxSetup({
        data: {
            '_token' : $('meta[name="csrf-token"]').attr('content')
        }
    });


    $("#cities").autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: '/suggestion/' + request.term,
                type: 'GET',
                success:function (res) {
                    response( res.cities );
                }
            });
        },
        select: function( event, ui ) {
            $('#map').addClass('loading');
            $.ajax({
                url: '/nearest/' + ui.item.value,
                type: 'GET',
                success:function (res) {
                    markersArray = res.cities;
                    initMap();
                    $('#map').removeClass('loading');
                }
            });
        }
    });



    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 7,
            center: {lat: markersArray[0].latitude, lng: markersArray[0].longitude}
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0; i < markersArray.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(markersArray[i].latitude, markersArray[i].longitude),
                map: map,
                title: markersArray[i].name

            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(markersArray[i].name);
                    infowindow.open(map, marker);
                }
            })(marker, i));

            if(i === 0) {
                infowindow.setContent(markersArray[i].name);
                infowindow.open(map, marker);
            }
        }
    }
});