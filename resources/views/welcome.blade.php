<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">

        <title>Laravel</title>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="container">
                <!--Make sure the form has the autocomplete function switched off:-->
                <div class="autocomplete">
                    <input id="cities" type="text" name="city" placeholder="City">
                </div>

                <div id="map"></div>
            </div>
        </div>

        <script src="{{ asset('js/app.js') }}"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9clyZgRCxMUVcWffP5Z41qjrBHRtorNw">
        </script>

        <script src="{{ asset('js/main.js') }}"></script>
    </body>
</html>
